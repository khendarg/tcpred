#!/usr/bin/env python

from __future__ import print_function

import os, tempfile, subprocess
import Bio.Blast.Applications
import Bio.Blast.NCBIXML

class Residue(object):
	def __init__(self, letter, confidences=None):
		self.letter = letter
		self.predictions = {'H':-1.000, 'E':-1.000, 'C':-1.000} if confidences is None else confidences

	def get_best(self):
		bestpred, bestconf = None, None
		for k in self.confidences:
			if bestconf is None:
				bestpred, bestconf = k, self.confidences[k]
			#TODO: do something about ties for best
			elif self.confidences[k] > bestconf:
				bestpred, bestconf = k, self.confidences[k]
		return bestpred

class Psipred(object):
	def __init__(self):
		self.seq = {}
		self.predictions = {}
		self.confidences = {}
		self.indices = []

	@staticmethod
	def parse(f):
		selfobj = Psipred()
		for l in f:
			if not l.strip(): continue
			elif l.startswith('#'): continue
			else:
				index, resname, pred, loop, helix, strand = l.strip().split()
				index = int(index)
				loop = float(loop)
				helix = float(helix)
				strand = float(strand)
				selfobj.seq[index] = resname
				selfobj.predictions[index] = pred
				selfobj.confidences[index] = [loop, helix, strand]
				selfobj.indices.append(index)
				#self.seq[n] =
		return selfobj

def psiblast(query, db, f_pssm, inclusion_ethresh=0.001, num_iterations=3,
	num_alignments=0, max_target_seqs=5000, outfmt=5):

	cline = Bio.Blast.Applications.NcbipsiblastCommandline(
		db=db, 
		out_pssm=f_pssm.name, 
		inclusion_ethresh=inclusion_ethresh, 
		num_iterations=3, 
		max_target_seqs=max_target_seqs,
		outfmt=5)

	pbxml, err = cline(stdin=query)
	f_xml = tempfile.NamedTemporaryFile()
	f_xml.write(pbxml)
	f_xml.flush()
	f_xml.seek(0)
	results = Bio.Blast.NCBIXML.parse(f_xml)

	return results

	
def run(fasta, db='pdbaa', psipred_datadir=None):
	if psipred_datadir is None:
		if 'PSIPRED_DATA' in os.environ: 
			psipred_datadir = os.environ['PSIPRED_DATA']
		else: raise IOError('Could not find PSIPRED data directory')
	assert os.path.isdir(psipred_datadir)

	#TODO: allow keeping temporary files

	out_pssm = tempfile.NamedTemporaryFile()
	hits = psiblast(db=db, query=fasta, f_pssm=out_pssm)

	matrix = subprocess.check_output(['chkparse', out_pssm.name])
	f_matrix = tempfile.NamedTemporaryFile()
	f_matrix.write(matrix)
	f_matrix.flush()

	#pass 1
	cmd1 = ['psipred', 
		f_matrix.name,
		'{}/weights.dat'.format(psipred_datadir),
		'{}/weights.dat2'.format(psipred_datadir),
		'{}/weights.dat3'.format(psipred_datadir),
	]
	ss1 = subprocess.check_output(cmd1)
	f_ss1 = tempfile.NamedTemporaryFile()
	f_ss1.write(ss1)
	f_ss1.flush()

	#pass 2
	f_ss2 = tempfile.NamedTemporaryFile()
	horiz = subprocess.check_output(['psipass2',
		'{}/weights_p2.dat'.format(psipred_datadir),
		'1', '1.0', '1.0',
		f_ss2.name,
		f_ss1.name
	])

	#print(horiz)

	f_ss2.seek(0)
	
	return Psipred.parse(f_ss2)
